FROM python:3-alpine
RUN apk add build-base linux-headers
RUN pip3 install flask psutil jinja2-humanize-extension gunicorn

COPY . .

CMD gunicorn -b :5000 dashing:app