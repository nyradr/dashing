from flask import Flask, render_template
import shutil
import psutil


def get_system_info():
    """
    Get system informations as json.
    Informations:
        - disk usage (total, used, free)
    :return:
    """
    total, used, free = shutil.disk_usage("/")

    memory = psutil.virtual_memory()

    return {
        'disk_total': total,
        'disk_used': used,
        'disk_used_pc': int(used / total * 100),
        'cpu_pc': psutil.cpu_percent(),
        'mem_total': memory.total,
        'mem_used': memory.used,
        'mem_pc': int(memory.used / memory.total * 100)
    }

def create_app():
    """
    Create the app and load the config from environment variables.
    If not environment available, get default debug values
    :return:
    """
    app = Flask(__name__)
    app.jinja_options['extensions'] = ['jinja2_humanize_extension.HumanizeExtension']

    app.config.from_mapping(
        SITE_NAME='nyradr.ovh',
    )

    @app.route('/')
    def main():  # put application's code here
        system = get_system_info()

        return render_template("main.html", config=app.config, system=system)

    return app


app = create_app()
